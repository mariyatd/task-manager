import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';

import {TaskDatagridComponent} from './task-datagrid.component';
import {TaskService} from '../../shared/services/task.service';
import { Task } from 'src/app/shared/models/task.model';
import {Observable, of} from 'rxjs';

describe('TaskDatagridComponent', () => {
  let component: TaskDatagridComponent;
  let fixture: ComponentFixture<TaskDatagridComponent>;

  let mockTaskService;
  let mockTask: Task[];

  class MockTaskService {
    getTasks() {
      return of([{
        id: 1,
        title: 'Title',
        description: 'descr'
      }]);
    }
  }

  beforeEach(async(() => {

    mockTaskService = jasmine.createSpyObj(['getTasks']);

    TestBed.configureTestingModule({
      declarations: [TaskDatagridComponent],
      imports: [HttpClientTestingModule, HttpClientModule, RouterTestingModule],
      providers: [{provide: TaskService, useClass: MockTaskService}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    mockTask = [{
      id: 1,
      title: 'Title',
      description: 'descr'
    }];

    fixture = TestBed.createComponent(TaskDatagridComponent);
    component = fixture.componentInstance;

    mockTaskService.getTasks.and.returnValue(of(mockTask));
    mockTaskService.hasLoadedObs = of(true);

    fixture.detectChanges();
  });

  it('should create',  async(inject([TaskService], (taskService: TaskService) => {
    expect(component).toBeTruthy();
  })));
});

// TODO test if delete button calls the service delete with right id
// TODO test if edit button loads task component
// TODO more tests
