import {Component, OnInit} from '@angular/core';
import {TaskService} from '../../shared/services/task.service';
import {Task} from 'src/app/shared/models/task.model';
import {ObjectDiffUtil} from 'src/app/shared/utils/objectDiffUtil';
import {Router} from '@angular/router';

@Component({
  selector: 'app-task-datagrid',
  templateUrl: './task-datagrid.component.html',
  styleUrls: ['./task-datagrid.component.scss']
})
export class TaskDatagridComponent implements OnInit {
  tasks;
  selectedTask: Task;

  constructor(private taskService: TaskService, private router: Router) {
  }

  ngOnInit(): void {
    this.taskService.getTasks().subscribe((data) => this.tasks = data);
  }

  onSelect(task: Task) {
    this.selectedTask = task;
  }

  onDelete(task: Task) {
    this.taskService
      .deleteTask(task.id)
      .subscribe(res => alert('Task ' + task.title + ' has been deleted'),
        error => {
          console.error('Error: ', error);
          alert('Error occurred: ' + error.error);
        },
        () => window.location.reload());
  }

  onSubmit(task: Task): void {
    const patchBody = ObjectDiffUtil.diff(this.selectedTask, task);

    this.taskService
      .updateTask(this.selectedTask.id, patchBody)
      .subscribe(res => alert('Task ' + res.title + ' has been updated'),
        error => {
          console.error('Error: ', error);
          alert('Error occurred: ' + error.error);
        },
        () => window.location.reload());
  }

  onCancel(): void {
    this.selectedTask = null;
  }
}
