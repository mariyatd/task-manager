import {Component, OnInit} from '@angular/core';
import {TaskService} from '../../shared/services/task.service';
import {Task} from '../../shared/models/task.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.scss']
})
export class TaskCreateComponent implements OnInit {

  constructor(private taskService: TaskService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit(task: Task): void {
    this.taskService.createTask(task)
      .subscribe(res => alert('Task ' + res.title + ' created successfully'),
        error => {
          console.error('Error: ', error);
          alert('Error occurred: ' + error.error);
        },
        () => this.router.navigate(['/tasks']));
  }

  onCancel(): void {
    this.router.navigate(['/tasks']);
  }
}
