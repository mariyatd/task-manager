import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskCreateComponent } from './task-create.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TaskService} from '../../shared/services/task.service';
import {HttpClientModule} from '@angular/common/http';
import {of} from 'rxjs';
import { Task } from 'src/app/shared/models/task.model';

describe('TaskCreateComponent', () => {
  let component: TaskCreateComponent;
  let fixture: ComponentFixture<TaskCreateComponent>;

  class MockTaskService {
    getTasks() {
      return of([{
        id: 1,
        title: 'Title',
        description: 'descr'
      }]);
    }

    createTask(task: Task) {
    }

    deleteTask(task: Task) {
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskCreateComponent ],
      imports: [HttpClientTestingModule, HttpClientModule, RouterTestingModule],
      providers: [{provide: TaskService, useClass: MockTaskService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // TODO tests
});
