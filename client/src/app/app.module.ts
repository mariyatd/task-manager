import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TaskService} from './shared/services/task.service';
import {HttpClientModule} from '@angular/common/http';
import {TaskFormComponent} from './shared/components/task-form/task-form.component';
import {TaskDatagridComponent} from './tasks/task-datagrid/task-datagrid.component';
import {TaskCreateComponent} from './tasks/task-create/task-create.component';
import {RouterModule} from '@angular/router';
import {ClarityModule} from '@clr/angular';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TaskFormComponent,
    TaskDatagridComponent,
    TaskCreateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot([
      {path: '', redirectTo: 'tasks', pathMatch: 'full'},
      {path: 'tasks', component: TaskDatagridComponent},
      {path: 'createTask', component: TaskCreateComponent}
    ]),
    ClarityModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [TaskService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
