import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TaskService } from './task.service';

describe('TaskService', () => {
  let taskService: TaskService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        TaskService
      ],
    });

    taskService = TestBed.get(TaskService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it(`should fetch tasks as an Observable`, async(inject([HttpTestingController, TaskService],
    (httpClient: HttpTestingController, taskService: TaskService) => {

      const taskItem = [
        {
          id: 1,
          title: 'Task1',
          description: 'boo'
        },
        {
          id: 2,
          title: 'Task2',
          description: 'booboo'
        },
        {
          id: 3,
          title: 'Task3',
          description: 'boobooboo'
        },
      ];


      taskService.getTasks()
        .subscribe((tasks: any) => {
          expect(tasks.length).toBe(3);
        });

      const req = httpMock.expectOne('http://localhost:1337/tasks');
      expect(req.request.method).toBe('GET');

      req.flush(taskItem);
      httpMock.verify();
    })));
});
