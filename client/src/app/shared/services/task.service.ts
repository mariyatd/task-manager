import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Task} from '../models/task.model';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class TaskService {
  private taskApiBaseUrl = 'http://localhost:1337/tasks';

  constructor(private http: HttpClient) {
  }

  getTasks(): Observable<{}> {
    return this.http.get(this.taskApiBaseUrl)
      .pipe(catchError(error => throwError(error)));
  }

  getTask(taskId: number): Observable<{}> {
    const taskUrl = this.taskApiBaseUrl + '/' + taskId;
    return this.http.get(taskUrl)
      .pipe(catchError(error => throwError(error)));
  }

  createTask(task: Task): Observable<Task> {
    return this.http.post<Task>(this.taskApiBaseUrl, task)
      .pipe(catchError(error => throwError(error)));
  }

  updateTask(taskId: number, fields: any): Observable<any> {
    const taskUrl = this.taskApiBaseUrl + '/' + taskId;
    return this.http.patch(taskUrl, fields)
      .pipe(catchError(error => throwError(error)));
  }

  deleteTask(taskId: number): Observable<{}> {
    const deleteUrl = this.taskApiBaseUrl + '/' + taskId;
    return this.http.delete(deleteUrl)
      .pipe(catchError(error => throwError(error)));
  }
}
