import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TaskFormComponent} from './task-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Task} from '../../models/task.model';

describe('TaskFormComponent', () => {
  let component: TaskFormComponent;
  let fixture: ComponentFixture<TaskFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
      ],
      declarations: [TaskFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have input value', () => {
    component.task = {id: 1,
      title: 'title'
    };
    component.ngOnInit();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('input[name=\'title\']').value).toEqual('title');
  });

  it('should have empty input field values', () => {
    component.task = null;
    component.ngOnInit();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('input[name=\'title\']').value).toEqual('');
    expect(fixture.nativeElement.querySelector('input[name=\'description\']').value).toEqual('');
  });


  it('should have valid title', () => {
    component.task = null;
    const titleInput = component.taskForm.controls.title;
    expect(titleInput.valid).toBeFalsy();

    titleInput.setValue('title');

    expect(titleInput.valid).toBeTruthy();

    titleInput.setValue('');

    expect(titleInput.valid).toBeFalsy();
  });


  it('should disable submit button when title is invalid', () => {
    component.task = null;
    component.submitButtonText = 'submit';
    const titleInput = component.taskForm.controls.title;

    expect(fixture.nativeElement.querySelector('button[type=\'submit\']').disabled).toBeTruthy();

    titleInput.setValue('title');

    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('button[type=\'submit\']').disabled).toBeFalsy();

    titleInput.setValue('');

    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('button[type=\'submit\']').disabled).toBeTruthy();
  });
});

// TODO test @Output
// TODO more tests
