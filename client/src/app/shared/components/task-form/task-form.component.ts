import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Task} from '../../models/task.model';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss']
})

export class TaskFormComponent implements OnInit {
  @Input() task: Task;
  @Input() submitButtonText;

  taskForm: FormGroup;

  @Output()
  taskReady = new EventEmitter<Task>();

  @Output()
  cancel = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.taskForm = this.formBuilder.group({
      title: [this.task?.title, Validators.required],
      description: [this.task?.description]
    });
  }

  onSubmit() {
    this.taskReady.emit(this.taskForm.value);
  }

  onCancel() {
    this.cancel.emit();
  }
}
