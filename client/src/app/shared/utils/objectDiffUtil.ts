export class ObjectDiffUtil {

  // FIXME this doesn't work for complex objects, arrays and all that jazz. Should be fixed if used in production!

   static diff(obj1, obj2) {
    const diffObject = {};
    let key;

    function compareByKey(item1, item2, itemKey) {
      if (item1 !== item2 ) {
        diffObject[itemKey] = item2;
      }
    }

    for (key in obj1) {
      if (obj1.hasOwnProperty(key)) {
        compareByKey(obj1[key], obj2[key], key);
      }
    }

    for (key in obj2) {
      if (obj2.hasOwnProperty(key)) {
        if (!obj1[key] && obj1[key] !== obj2[key] ) {
          diffObject[key] = obj2[key];
        }
      }
    }

    return diffObject;
  }
}
