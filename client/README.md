# Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

## How to run the project

Run `npm install` to install dependencies and run `npm run start` or `ng serve`. 
Navigate to `http://localhost:4200/`. For a sample of the unit tests (there are only a few), run `ng-test`.

Please note that I had some issues with the server, so I changed the CORS settings. They are in the Server folder.

## Brief explanation how I solved the task

I analysed the problem, ran the server and experimented with the api a bit using Postman to ensure I understand correctly.
After that I created the project structure, model and service for the tasks, as well as a reusable form component and other components.
I refactored the code to use reactive forms as I found it more suitable.
I added some tests just for a sample, more definitely need to be added.
Finally added some error handling for the CRUD operations.
For tasks that were bigger I worked in a separate branch.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
